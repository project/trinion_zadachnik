<?php

declare(strict_types=1);

namespace Drupal\trinion_zadachnik\Plugin\EntityReferenceSelection;

use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;
use Drupal\taxonomy\Entity\Term;
use Drupal\taxonomy\Plugin\EntityReferenceSelection\TermSelection;

/**
 * @todo Add plugin description here.
 *
 * @EntityReferenceSelection(
 *   id = "trinion_zadachnik_task_category_by_project_selection",
 *   label = @Translation("Task category by project selection"),
 *   group = "trinion_zadachnik_task_category_by_project_selection",
 *   entity_types = {"taxonomy_term"},
 * )
 */
final class TaskCategoryByProjectSelection extends TermSelection {

  /**
   * {@inheritdoc}
   */
  protected function buildEntityQuery($match = NULL, $match_operator = 'CONTAINS'): QueryInterface {
    $query = parent::buildEntityQuery($match, $match_operator);
    return $query;
  }

  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    return [
      '#markup' => t('Work only for Tasks'),
    ];
  }

  public function getReferenceableEntities($match = NULL, $match_operator = 'CONTAINS', $limit = 0) {
    if ($project_id = \Drupal::request()->get('field_tz_proekt'));
    else {
      $route_match = \Drupal::routeMatch();
      $route_name = $route_match->getRouteName();
      if ($route_name == 'entity.node.edit_form') {
        $node = $route_match->getParameter('node');
        if ($project = Node::load($node->get('field_tz_proekt')->getString()))
          $project_id = $project->id();
      }
    }
    if (!empty($project_id)) {
      $query = \Drupal::entityQuery('taxonomy_term')
        ->condition('vid', 'kategoriya_zadachi')
        ->condition('field_tz_proekt', $project_id);
      $res = $query->accessCheck()->execute();
      $response = [];
      if ($res) {
        foreach (Term::loadMultiple($res) as $category)
          $response[$category->id()] = $category->label();
      }
      return ['kategoriya_zadachi' => $response];
    }
    return [];
  }
}

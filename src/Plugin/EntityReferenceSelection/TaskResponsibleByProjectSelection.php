<?php

declare(strict_types=1);

namespace Drupal\trinion_zadachnik\Plugin\EntityReferenceSelection;

use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;
use Drupal\taxonomy\Entity\Term;
use Drupal\user\Plugin\EntityReferenceSelection\UserSelection;

/**
 * @todo Add plugin description here.
 *
 * @EntityReferenceSelection(
 *   id = "trinion_zadachnik_task_responsible_by_project_selection",
 *   label = @Translation("Task responsible by project selection"),
 *   group = "trinion_zadachnik_task_responsible_by_project_selection",
 *   entity_types = {"user"},
 * )
 */
final class TaskResponsibleByProjectSelection extends UserSelection {

  /**
   * {@inheritdoc}
   */
  protected function buildEntityQuery($match = NULL, $match_operator = 'CONTAINS'): QueryInterface {
    $query = parent::buildEntityQuery($match, $match_operator);
    return $query;
  }

  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    return [
      '#markup' => t('Work only for Tasks'),
    ];
  }

  public function getReferenceableEntities($match = NULL, $match_operator = 'CONTAINS', $limit = 0) {
    $route_name = \Drupal::routeMatch()->getRouteName();
    if ($route_name == 'node.add' && !empty($_COOKIE['proekt']) && is_numeric($_COOKIE['proekt']) && $_COOKIE['proekt'] != \Drupal::config('trinion_zadachnik.settings')->get('project_nerazobrannaya_tid')) {
      $project_id = $_COOKIE['proekt'];
    }
    else {
      if ($project_id = \Drupal::request()->get('field_tz_proekt')) ;
      else {
        $route_match = \Drupal::routeMatch();
        $route_name = $route_match->getRouteName();
        if ($route_name == 'entity.node.edit_form') {
          $node = $route_match->getParameter('node');
          if ($project = Term::load($node->get('field_tz_proekt')->getString()))
            $project_id = $project->id();
        }
      }
    }
    if (!empty($project_id)) {
      $query = \Drupal::entityQuery('user')
        ->condition('field_tz_proekt', $project_id)
        ->condition('roles', 't_zadachnik');
      $res = $query->accessCheck()->execute();
      if ($res) {
        foreach (\Drupal\user\Entity\User::loadMultiple($res) as $user) {
          $nick = $user->get('field_tb_nick_name')->getString();
          $options_resp[$user->id()] = $nick != '' ? $nick : $user->label();
        }
      }
      if (!empty($options_resp))
        return ['user' => $options_resp];
    }
    return [];
  }

}

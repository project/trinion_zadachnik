<?php
namespace Drupal\trinion_zadachnik\Plugin\TrinionWidget;

use Drupal\taxonomy\Entity\Term;
use Drupal\trinion_base\WidgetPluginBase;
use Drupal\trion_base\Annotation\Widget;
use Drupal\views\Views;

/**
 * @Widget(
 *   id="my_task",
 *   title="Tasks",
 *   type="pie_my_all"
 * )
 */
class MyTasksPlugin extends WidgetPluginBase {
  public function getData($options) {
    $my = isset($options['my']) && $options['my'] == 'true' ? TRUE : FALSE;
    $view = Views::getView('zadachi_naznachennye_mne');
    $view->setDisplay($my  ? 'widget_my_task' : 'widget_my_projects');
    $view->execute();
    $data = [];
    foreach ($view->result as $row) {
      $term = Term::load($row->node__field_tz_status_zadachi_field_tz_status_zadachi_target);
      $config = \Drupal::config('trinion_zadachnik.settings');
      switch ($term->id()) {
        case $config->get('status_novaya_tid'):
          $color = '#f2f234';
          break;
        case $config->get('status_v_rabote_tid'):
          $color = '#008000';
          break;
        case $config->get('status_vipolnenae_tid'):
          $color = '#018fd5';
          break;
        case $config->get('status_zakrita_tid'):
          $color = '#848484';
          break;
        case $config->get('status_na_dorabotku_tid'):
          $color = '#ff0000';
          break;
        case $config->get('status_otlozheno_tid'):
          $color = '#000000';
          break;
        case $config->get('status_nuzhen_otklik_tid'):
          $color = '#f9a306';
          break;
        case $config->get('status_otmenena_tid'):
          $color = '#fde2af';
          break;
        default:
          $color = '#' . str_pad(dechex(mt_rand(0, 0xFFFFFF)), 6, '0', STR_PAD_LEFT);
      }
      $data[$term->label()] = [
        'cnt' => $row->nid,
        'color' => $color,
      ];
    }
    return  $data;
  }

  public function getWeight() {
    return 1;
  }

}


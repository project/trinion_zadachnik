<?php

declare(strict_types=1);

namespace Drupal\trinion_zadachnik\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;
use Drupal\trinion_bpmn_import\Controller\BusinessProcessController;

final class ChangeTaskStatusForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'trinion_zadachnik_change_task_status_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $task = $form_state->getBuildInfo()['args'][0];

    $form['actions'] = [
      '#type' => 'actions',
      '#no-mt' => TRUE,
    ];

    $form['#action'] = '/node/' . $task->id();

    if ($time = $task->get('field_tz_vrem_vypoleniya_zadachi')->getString()) {
      $start_date = new \DateTime('now');
      $diff = $start_date->diff(new \DateTime($time));
      $time = [];
      if ($diff->y)
        $time[] = t('%years years', ['%years' => $diff->y]);
      if ($diff->m)
        $time[] = t('%months months', ['%months' => $diff->m]);
      if ($diff->d)
        $time[] = t('%days days', ['%days' => $diff->d]);
      if ($diff->h)
        $time[] = t('%hours hours', ['%hours' => $diff->h]);
      if ($diff->i)
        $time[] = t('%minutes minutes', ['%minutes' => $diff->i]);
      $form['counter'] = [
        '#markup' => implode(' ', $time),
      ];
    }
    if ($task->get('field_tz_status_zadachi')->getString() == \Drupal::config('trinion_zadachnik.settings')->get('status_novaya_tid')) {
      $form['actions'][] = [
        '#type' => 'submit',
        '#value' => t('Complete task'),
        '#custom_suggestion' => 'approve_btn',
      ];
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $task = $form_state->getBuildInfo()['args'][0];
    $task->field_tz_status_zadachi = \Drupal::config('trinion_zadachnik.settings')->get('status_vipolnenae_tid');
    $task->save();
  }
}

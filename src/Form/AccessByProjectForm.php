<?php

namespace Drupal\trinion_zadachnik\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\taxonomy\Entity\Term;
use Drupal\user\Entity\User;

/**
 * Provides the access task by projects administration form.
 *
 * @internal
 */
class AccessByProjectForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'trinion_zadachnik_project_task_access';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $tid = \Drupal::request()->get('project');
    if (empty($tid)) {
      $data = [];
      $theme_handler = \Drupal::service('theme_handler');
      $theme_path = $theme_handler->getTheme('trinion_backend')->getPath();
      $path =  $theme_path . '/templates/svg/edit.svg';
      $svg = file_get_contents($path);

      foreach ($this->getProjects() as $project) {
        if ($project->tid == \Drupal::config('trinion_zadachnik.settings')->get('project_nerazobrannaya_tid'))
          continue;
        $data[] = [
          $project->name,
          \Drupal::linkGenerator()->generate(['#type' => 'inline_template', '#template' => $svg], Url::fromRoute('trinion_zadachnik.task_by_project_access', [], ['query' => ['project' => $project->tid]])),
        ];
      }

      $form['projects'] = [
        '#theme' => 'table',
        '#header' => ['Project', '',],
        '#rows' => $data,
      ];
      return $form;
    }
    else {
      $form['project'] = [
        '#type' => 'hidden',
        '#value' => $tid,
      ];

      $config = \Drupal::state()->get('trinion_zadachnik_project_task_access');
      $zadachnik_users = [];
      $query = \Drupal::entityQuery('user')
        ->condition('roles', 't_zadachnik');
      $res = $query->accessCheck()->execute();
      if ($res) {
        foreach (User::loadMultiple($res) as $zadachnik_user) {
          foreach ($zadachnik_user->get('field_tz_proekt') as $item)
            if ($item->getString() == $tid)
              continue 2;
          $zadachnik_users[$zadachnik_user->id()] = \Drupal::service('trinion_main.helper')->getNameOrLogin($zadachnik_user);
        }
      }
      $form['select_user'] = [
        '#type' => 'select',
        '#options' => $zadachnik_users,
        '#empty_option' => t('Select user to add to this project'),
      ];
      $form['permissions'] = [
        '#type' => 'table',
        '#header' => [$this->t('Permission')],
        '#id' => 'task-projects-permissions',
      ];
      $ops = ['view any', 'edit any',];
      $form['permissions']['#header'] = ['', 'View all', 'Edit all', '', ];

      $users = $this->getUsersByProject($tid);

      foreach ($users as $uid => $user_name) {
        $form['permissions'][$uid]['description']['#markup'] = $user_name;
        foreach ($ops as $op) {
          $form['permissions'][$uid][$op] = [
            '#type' => 'checkbox',
            '#default_value' => empty($config[$tid][$uid][$op]) ? 0 : 1,
          ];
        }

        $form['permissions'][$uid]['remove_' . $uid] = [
          '#type' => 'submit',
          '#name' => 'remove_' . $uid,
          '#value' => $this->t('Remove'),
        ];
      }
    }
    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    ];
    return $form;
  }

  public function getUsersByProject($project_tid) {
    $query = \Drupal::entityQuery('user')
      ->condition('field_tz_proekt', $project_tid);
    $res = $query->accessCheck()->execute();
    $users = [];
    foreach ($res as $uid) {
      $user = User::load($uid);
      $users[$uid] = \Drupal::service('trinion_main.helper')->getNameOrLogin($user);
    }
    return $users;
  }

  public function getProjects() {
    return \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree('tz_proekt', 0, 1, FALSE);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    if (preg_match('/edit-permissions-\d+-remove-(\d+)/', $form_state->getTriggeringElement()['#id'], $match)) {
      $user_to_del = $match[1];
      $user = User::load($user_to_del);
      $projects = [];
      foreach ($user->get('field_tz_proekt') as $proekt) {
        $proekt = $proekt->getString();
        if ($proekt != $values['project']) {
          $projects[] = $proekt;
        }
        $user->set('field_tz_proekt', $projects);
      }
      $user->save();
    }

    if ($values['select_user']) {
      $user = User::load($values['select_user']);
      foreach ($user->get('field_tz_proekt') as $proekt)
        if ($proekt->getString() == $values['project'])
          $exists = TRUE;
      if (empty($exists)) {
        $user->field_tz_proekt[] = $values['project'];
        $user->save();
      }
    }

    $config = \Drupal::state()->get('trinion_zadachnik_project_task_access');
    $config[$values['project']] = $values['permissions'];
    \Drupal::state()->set('trinion_zadachnik_project_task_access', $config);
    $this->messenger()->addStatus($this->t('The changes have been saved.'));
  }

  public function getPageTitle() {
    if (($tid = \Drupal::request()->get('project')) && $project = Term::load($tid)) {
      $title = t('Project permissions: @project', ['@project' => $project->label()]);
    }
    else {
      $title = t('Project permissions');
    }
    return $title;
  }
}

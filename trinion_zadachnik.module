<?php

/**
 * Implements hook_form_alter().
 */
function trinion_zadachnik_form_alter(&$form, \Drupal\Core\Form\FormStateInterface &$form_state, $form_id) {
  if ($form_id == 'node_zadacha_form') {
    if ($base_document = Drupal::request()->get('base-document')) {
      if ($node = \Drupal\node\Entity\Node::load($base_document)) {
        $bundle = $node->bundle();
        $doc_name = \Drupal\node\Entity\NodeType::load($bundle)->label();
        $title = t($doc_name) . ' №' . $node->label() . ',';
        $form['field_tz_tema']['widget'][0]['value']['#default_value'] = $title;
        $form['field_tz_ssylka_na_obekt']['widget'][0]['value']['#default_value'] = $node->toUrl()->toString();
      }
    }
    $config = Drupal::config('trinion_zadachnik.settings');
    if ($default_value = $config->get('default_activity'))
      $form['field_tz_deyatelnost']['widget']['#default_value'] = $default_value;
    if ($default_value = $config->get('default_prioritet'))
      $form['field_tz_prioritet']['widget']['#default_value'] = $default_value;
    if (!empty($_COOKIE['proekt']) && is_numeric($_COOKIE['proekt']) && $_COOKIE['proekt'] != \Drupal::config('trinion_zadachnik.settings')->get('project_nerazobrannaya_tid'))
      $form['field_tz_proekt']['widget']['#default_value'] = $_COOKIE['proekt'];
  }
  if (in_array($form_id, ['taxonomy_term_tz_proekt_form', 'taxonomy_term_prioritet_zadachi_form', 'taxonomy_term_kategoriya_zadachi_form', 'taxonomy_term_tz_statusy_zadach_form', 'taxonomy_term_deatelnost_zadacha_form'])) {
    $form['description']['#access'] = FALSE;
    $form['relations']['#access'] = FALSE;
    $form['status']['#access'] = FALSE;
  }
  if ($form_id == 'node_zadacha_edit_form') {
    $form['change_comment'] = [
      '#type' => 'textarea',
      '#title' => t('Comment on the change'),
      '#weight' => 99,
    ];
  }
  if ($form_id == 'node_zadacha_form' || $form_id == 'node_zadacha_edit_form') {
    unset($form['field_tz_role']['widget']['#options']['administrator'],$form['field_tz_role']['widget']['#options']['anonymous'],$form['field_tz_role']['widget']['#options']['authenticated']);
    $form['field_tz_proekt']['widget']['#ajax'] = [
      'callback' => 'trinion_zadachnik_field_tz_proekt_callback',
      'method' => 'replace',
      'event' => 'change',
    ];
    $form['#validate'][] = 'trinion_zadachnik_zadacha_task_form_validate';
    $form['#after_build'] = ['trinion_zadachnik_node_zadacha_form'];
  }
  elseif ($form_id == 'comment_tz_k_zadache_form') {
    $route_match = Drupal::routeMatch();
    if ($route_match->getRouteName() == 'entity.node.canonical') {
      if ($route_match->getParameter('node')->bundle() != 'zadacha')
        $form['field_tz_private']['#access'] = FALSE;
      elseif (!Drupal::service('trinion_zadachnik.helper')->isUserSotrudnik())
        $form['field_tz_private']['#access'] = FALSE;
    }
    $form['#after_build'] = ['trinion_zadachnik_comment_tz_k_zadache_form'];
  }
}

function trinion_zadachnik_comment_tz_k_zadache_form($form, \Drupal\Core\Form\FormState $form_state) {
  unset($form['field_tz_kommentariy']['widget'][0]['format']['help']);
  return $form;
}

function trinion_zadachnik_node_zadacha_form($form, \Drupal\Core\Form\FormState $form_state) {
  unset($form['field_tz_opisanie']['widget'][0]['format']['help']);
  return $form;
}

function trinion_zadachnik_field_tz_proekt_callback($form, \Drupal\Core\Form\FormState $form_state) {
  $response = new \Drupal\Core\Ajax\AjaxResponse();
  $kategoriya = $form['field_tz_kategoriya_zadachi'];
  $otvetstvennyy = $form['field_tz_otvetstvennyy'];
  $related_task = $form['field_tz_related_tasks'];
  $response->addCommand(new \Drupal\Core\Ajax\InsertCommand('.field--name-field-tz-kategoriya-zadachi', $kategoriya));
  $response->addCommand(new \Drupal\Core\Ajax\InsertCommand('.field--name-field-tz-otvetstvennyy', $otvetstvennyy));
  $response->addCommand(new \Drupal\Core\Ajax\InsertCommand('.field--name-field-tz-related-tasks', $related_task));
  return $response;
}

/**
 * Implements hook_preprocess_HOOK().
 */
function trinion_zadachnik_preprocess_views_view_field(&$variables) {
  if ($variables['field']->field == 'field_tz_otvetstvennyy') {
    $items = [];
    foreach ($variables['row']->_entity->get('field_tz_otvetstvennyy') as $item) {
      if ($user = \Drupal\user\Entity\User::load($item->entity->id())) {
        $items[] = $user->get('field_tb_nick_name')->getString();
      }
    }
    $variables['output'] = implode(', ', $items);
  }
}

function trinion_zadachnik_zadacha_task_form_validate($form, \Drupal\Core\Form\FormState &$form_state) {
  $user_selected = $role_selected = FALSE;
  $otvetstvennyy = $form_state->getValue('field_tz_otvetstvennyy');
  if ($otvetstvennyy) {
    foreach ($otvetstvennyy as $key => $item) {
      if (is_numeric($key)) {
        if (!is_null($item['target_id'])) {
          $user_selected = TRUE;
          break;
        }
      }
    }
  }
//  foreach ($form_state->getValue('field_tz_role') as $key => $item) {
//    if (is_numeric($key)) {
//      if (!is_null($item['target_id'])) {
//        $role_selected = TRUE;
//        break;
//      }
//    }
//  }
  if (!$user_selected && !$role_selected)
    $form_state->setErrorByName('', t('Be sure to fill in either the Responsible field or the Role.'));
}

/**
 * Implements hook_entity_extra_field_info().
 */
function trinion_zadachnik_entity_extra_field_info() {
  $extra = [];

  $extra['node']['zadacha'] = [
    'display' => [
      'knopka_smeni_statusa_zadachi' => [
        'label' => t('Change task status form'),
        'weight' => '100'
      ]
    ]
  ];
  return $extra;
}

function trinion_zadachnik_node_view(array &$build, \Drupal\node\NodeInterface $node, $display, $view_mode) {//dump($node->id());
  $bundle = $node->bundle();
  if ($bundle == 'zadacha') {
    if ($node->get('field_tz_status_zadachi')->getString() == Drupal::config('trinion_zadachnik.settings')->get('status_novaya_tid')) {
      $build['#cache']['max-age'] = 0;
      if ($node->get('field_tbi_ishodniy_element')->getString() == '')
        $build['knopka_smeni_statusa_zadachi'] = \Drupal::formBuilder()->getForm('Drupal\trinion_zadachnik\Form\ChangeTaskStatusForm', $node);
    }
  }
}

/**
 * Implements hook_entity_load().
 */
function trinion_zadachnik_entity_load($entities, $entity_type_id) {
  if ($entity_type_id == 'node') {
    foreach ($entities as $entity) {
      $bundle = $entity->bundle();
      if ($bundle == 'zadacha') {
        $entity->node_title = t('@document № @num from @date', [
            '@document' => $entity->get('type')->first()->entity->label(),
            '@num' => $entity->label(),
            '@date' => date('j.m.Y H:i', $entity->get('created')->getString()),
          ]
        );
      }
    }
  }
}

/**
 * Implements hook_entity_access().
 */
function trinion_zadachnik_entity_access(\Drupal\Core\Entity\EntityInterface $entity, $operation, \Drupal\Core\Session\AccountInterface $account) {
  $bundle = $entity->bundle();
  if ($bundle == 'zadacha') {
    if ($operation == 'update') {
      $allowed_users = [$entity->get('uid')->getString()];
      $uid = $account->id();

      $user = \Drupal\user\Entity\User::load($uid);
      $tids = [0];
      foreach ($user->get('field_tz_proekt') as $item) {
        $tids[] = $item->entity->id();
      }
      $project_tid = $entity->get('field_tz_proekt')->getString();
      if (in_array($project_tid, $tids))
        if (Drupal::service('trinion_zadachnik.helper')->accessUserToProject($uid, $project_tid, 'edit any'))
          $allowed_users[] = $uid;

      foreach ($entity->get('field_tz_otvetstvennyy') as $item) {
        $allowed_users[] = $item->getString();
      }
      if ($entity->get('uid')->getString() != $uid) {
        if ($user->get('field_tz_redaktirovat_zadachi')->getString() == 1) {
          if (in_array($uid, $allowed_users)) {
            return \Drupal\Core\Access\AccessResult::allowed();
          }
        }
      }
    }
    elseif ($operation == 'view') {
      $allowed_users = [$entity->get('uid')->getString()];
      $uid = $account->id();

      $user = \Drupal\user\Entity\User::load($uid);
      $tids = [0];
      foreach ($user->get('field_tz_proekt') as $item) {
        $tids[] = $item->entity->id();
      }
      $project_tid = $entity->get('field_tz_proekt')->getString();
      if (in_array($project_tid, $tids))
        if (Drupal::service('trinion_zadachnik.helper')->accessUserToProject($uid, $project_tid, 'view any'))
          $allowed_users[] = $uid;

      foreach ($entity->get('field_tz_otvetstvennyy') as $item) {
        $allowed_users[] = $item->getString();
      }
      return \Drupal\Core\Access\AccessResult::forbiddenIf(!in_array($uid, $allowed_users));
    }
  }
}

/**
 * Implements hook_entity_presave().
 */
function trinion_zadachnik_entity_presave(Drupal\Core\Entity\EntityInterface $entity) {
  $bundle = $entity->bundle();
  $entity_type = $entity->getEntityType()->id();
  if ($entity_type == 'comment' && $bundle == 'tz_k_zadache') {
    if ($entity->getCommentedEntity()->bundle() == 'zadacha')
      Drupal::service('trinion_main.mails')->uvedomlenieNoviyCommentKZadache($entity);
  }
  if ($bundle == 'zadacha') {
    if ($entity->isNew()) {
      $query = \Drupal::database()->select('node_field_data')
        ->condition('type', $bundle);
      $query->addExpression('CAST(title as SIGNED)', 'num');
      $query->orderBy('num', 'DESC');
      $query->range(0, 1);
      if (!($last = $query->execute()->fetchField()))
        $last = 0;
      $entity->title = $last + 1;
    }
    if ($days_to_complete = $entity->get('field_tz_dney_na_vypolnenie')->getString()) {
      $start_time = $entity->get('field_tz_vremya_nachala_zadachi')->getString();
      if (empty($start_time))
        $start_time = date('Y-m-d H:i:s');
      $hours_to_complete = round($days_to_complete * 24);
      $entity->field_tz_vrem_vypoleniya_zadachi = date('Y-m-d\TH:i:s', strtotime($hours_to_complete . ' hours', strtotime($start_time)));
    }
  }
}

/**
 * Implements hook_entity_insert().
 */
function trinion_zadachnik_entity_insert(\Drupal\Core\Entity\EntityInterface $entity) {
  $bundle = $entity->bundle();
  if ($bundle == 'zadacha') {
    Drupal::service('trinion_main.mails')->uvedomlenieNovayaZadacha($entity);
    $text = t('New task') . '<a href="/node/' . $entity->id() . '">#' . $entity->label() . '</a> ' . $entity->get('field_tz_tema')->getString();
    foreach ($entity->get('field_tz_otvetstvennyy') as $user) {
      \Drupal\trinion_base\Controller\NoticeController::uvedomlenie($user->getString(), $text);
    }
  }
  elseif ($bundle == 'change_log') {
    if ($entity->field_tcl_object->first()->entity->bundle() == 'zadacha')
      Drupal::service('trinion_main.mails')->uvedomlenieObIzmeneniiZadachi($entity);
    $changelog_comment = Drupal::request()->request->get('change_comment');
    if (!empty($changelog_comment) && trim($changelog_comment) != '') {
      $task_nid = Drupal::routeMatch()->getParameter('node')->id();
      $comment = Drupal::service('trinion_zadachnik.helper')->createComment($task_nid, $changelog_comment);
      $comment->field_tz_ssilka_na_izmenenie = $entity->id();
      $comment->save();
    }
  }
  elseif ($bundle == 'mail') {
    Drupal::service("trinion_zadachnik.mail_processor")->processMailMessage($entity);
  }
}

/**
 * Implements hook_views_query_alter().
 */
function trinion_zadachnik_views_query_alter(\Drupal\views\ViewExecutable $view, \Drupal\views\Plugin\views\query\QueryPluginBase $query) {
  if ($view->id() == 'ssylka_na_suschnost_proekty_v_zadache') {
    $user = Drupal::currentUser();
    $user = \Drupal\user\Entity\User::load($user->id());
    $tids = [0];
    foreach ($user->get('field_tz_proekt') as $item)
      $tids[] = $item->entity->id();
    $query->addWhere(0, 'taxonomy_term_field_data.tid', $tids, 'IN');
  }
  if ($view->id() == 'zadachi_naznachennye_mne') {
    $selected_status = Drupal::request()->get('field_tz_status_zadachi_target_id');
    if (is_null($selected_status)) {
      $closed_tids = [];
      $closed_tids[] = Drupal::config('trinion_zadachnik.settings')->get('status_zakrita_tid');
      $closed_tids[] = Drupal::config('trinion_zadachnik.settings')->get('status_otmenena_tid');
      $query->addWhere(0, 'node__field_tz_status_zadachi.field_tz_status_zadachi_target_id', $closed_tids, 'NOT IN');
    }
  }
}

/**
 * Implements hook_views_post_build().
 */
function trinion_zadachnik_views_pre_build(\Drupal\views\ViewExecutable $view) {
  $view_id = $view->id();
  if ($view_id == 'zadachi_naznachennye_mne' && !empty($_COOKIE['proekt'])) {
    $view->setExposedInput(['field_tz_proekt_target_id' => $_COOKIE['proekt']]);
  }
}
/**
 * Implements hook_views_pre_execute().
 */
function trinion_zadachnik_views_pre_execute(\Drupal\views\ViewExecutable $view) {
  $view_id = $view->id();
  if ($view_id == 'zadachi_naznachennye_mne') {
    $show_only_my = Drupal::service('trinion_main.helper')->getMyAllSwitcherValue($view->id());
    if ($show_only_my || $view->current_display == 'widget_my_task') {
      // filter by Responsible and Role
      $roles = Drupal::currentUser()->getRoles(TRUE);
      $roles_assoc = [
        ':empty' => 'empty',
      ];
      foreach ($roles as $role) {
        $roles_assoc[':' . $role] = $role;
      }
      $roles_place_holder = implode(',', array_keys($roles_assoc));
      $query = $view->build_info['query'];
      $uid = Drupal::currentUser()->id();
      $query->leftJoin('node__field_tz_role', 'r', 'node_field_data.nid = r.entity_id AND r.field_tz_role_target_id IN (' . $roles_place_holder . ')', $roles_assoc);
      $query->leftJoin('node__field_tz_otvetstvennyy', 'ru', 'node_field_data.nid = ru.entity_id AND ru.field_tz_otvetstvennyy_target_id = :uid', [':uid' => $uid]);
      $or = $query->orConditionGroup();
      $or->isNotNull('r.entity_id');
      $or->isNotNull('ru.entity_id');
      $query->condition($or);
      $view->build_info['query'] = $query;
      $view->build_info['count_query'] = $query;
    }
    elseif (!$show_only_my || $view->current_display == 'widget_my_projects') {
      $uid = Drupal::currentUser()->id();
      $user = \Drupal\user\Entity\User::load($uid);
      $projects[] = 0;
      foreach ($user->get('field_tz_proekt') as $item) {
        $project_id = $item->entity->id();
        if (Drupal::service('trinion_zadachnik.helper')->accessUserToProject($uid, $project_id, 'view any')) {
          $projects[] = $project_id;
        }
      }
      $query = $view->build_info['query'];
      $query->leftJoin('node__field_tz_otvetstvennyy', 'ru', 'node_field_data.nid = ru.entity_id AND ru.field_tz_otvetstvennyy_target_id = :uid', [':uid' => $uid]);
      $or = $query->orConditionGroup();
      $or->isNotNull('ru.entity_id');
      $or->condition('node_field_data.uid', $uid);
      // filter by projects
      $or->condition('node__field_tz_proekt.field_tz_proekt_target_id', $projects, 'IN');
      $query->condition($or);
      $view->build_info['query'] = $query;
      $view->build_info['count_query'] = $query;
    }
  }
  elseif ($view_id == 'trinion_terms') {
    $query = $view->build_info['query'];
    $query->condition('tid', Drupal::config('trinion_zadachnik.settings')->get('project_nerazobrannaya_tid'), '<>');
  }
}

/**
 * Implements hook_query_alter().
 */
function trinion_zadachnik_query_alter(Drupal\Core\Database\Query\AlterableInterface $query) {
  if ($query->hasTag('entity_reference')) {
    // фильтр пользователей так же по проекту
    $project = Drupal::request()->get('project');
    if (!is_null($project) && $project) {
      $project_id = 0;
      if (preg_match('/\((.*?)\)\s*/', $project, $match)) {
        $project_id = $match[1];
      }
      $query->join('user__field_tz_proekt', 'pr', 'pr.entity_id = base_table.uid');
      $query->condition('pr.field_tz_proekt_target_id', $project_id);
    }
  }
}

/**
 * Implements hook_trinion_base_dashboard_widget().
 */
function trinion_zadachnik_trinion_base_dashboard_widget() {
  $widget = new \Drupal\trinion_base\Widget();
  $widget->setType('pie');
  $widget->setTitle(t('My tasks'));
  $widget->setWeight(1);
  $view = \Drupal\views\Views::getView('zadachi_naznachennye_mne');
  $view->setDisplay('widget_my_task');
  $view->execute();
  $data = [];
  foreach ($view->result as $row) {
    $term = \Drupal\taxonomy\Entity\Term::load($row->node__field_tz_status_zadachi_field_tz_status_zadachi_target);
    $config = Drupal::config('trinion_zadachnik.settings');
    switch ($term->id()) {
      case $config->get('status_novaya_tid'):
        $color = '#f2f234';
        break;
      case $config->get('status_v_rabote_tid'):
        $color = '#008000';
        break;
      case $config->get('status_vipolnenae_tid'):
        $color = '#018fd5';
        break;
      case $config->get('status_zakrita_tid'):
        $color = '#848484';
        break;
      case $config->get('status_na_dorabotku_tid'):
        $color = '#ff0000';
        break;
      case $config->get('status_otlozheno_tid'):
        $color = '#000000';
        break;
      case $config->get('status_nuzhen_otklik_tid'):
        $color = '#f9a306';
        break;
      case $config->get('status_otmenena_tid'):
        $color = '#fde2af';
        break;
      default:
        $color = '#' . str_pad(dechex(mt_rand(0, 0xFFFFFF)), 6, '0', STR_PAD_LEFT);
    }
    $data[$term->label()] = [
      'cnt' => $row->nid,
      'color' => $color,
    ];
  }
  $widget->setData($data);
  $widgets[] = $widget;

  return $widgets;
}
